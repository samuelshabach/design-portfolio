import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ScrollToTop from "./components/ScollTop/ScrollToTop";
import Navbar from "./components/Navbar/navbar";
import Homepage from "./pages/homepage/homepage";
import DesignList from "./pages/projects/designList";
import DevList from "./pages/DevList/devlist";
import GeekPage from "./pages/geek-page/geek";
import TeenPage from "./pages/TeenPage/teen";
import SnekPage from "./pages/SnekPage/snek";
import LearningList from "./pages/LearningList/learningList";
import LessonsWW1 from "./pages/LearningList/Lessons_WW1/Lessons";
// import Footer from "./components/Footer/Footer";

function App() {
  return (
    <>
      <Router>
        <ScrollToTop />
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Homepage />
          </Route>
          <Route exact path="/designs">
            <DesignList />
          </Route>
          <Route exact path="/development">
            <DevList />
          </Route>
          <Route exact path="/learning">
            <LearningList />
          </Route>
          <Route exact path="/designs/geeks">
            <GeekPage />
          </Route>
          <Route exact path="/designs/snek">
            <SnekPage />
          </Route>
          <Route exact path="/designs/teenlearner">
            <TeenPage />
          </Route>
          <Route exact path="/learning/lessons_from_WW1">
            <LessonsWW1 />
          </Route>
        </Switch>
        {/* <Footer /> */}
      </Router>
    </>
  );
}

export default App;
