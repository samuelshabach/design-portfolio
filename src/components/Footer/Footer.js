import "../Footer/Footer.css";
import {
  AiFillGithub,
  AiFillTwitterSquare,
  AiFillBehanceSquare,
  AiFillLinkedin,
  AiFillMail,
} from "react-icons/ai";
const Footer = () => {
  return (
    <div className="container">
      {/*  */}
      <div className="w-100 d-flex">
        <div className="d-flex m-auto">
          <a
            href="https://github.com/samuelshabach"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillGithub className="icon-size" />
          </a>

          {/* twitter */}
          <a
            className="px-4 mb-2"
            href="https://twitter.com/samuelshabach"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillTwitterSquare className="icon-size" />
          </a>

          {/* behance */}
          <a
            href="https://www.behance.net/samueladebayo2"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillBehanceSquare className="icon-size" />
          </a>

          {/* linked in */}
          <a
            className="px-4 mb-2"
            href="https://www.linkedin.com/in/samuel-adebayo-68ab5917a/"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillLinkedin className="icon-size" />
          </a>

          {/* email */}
          <a
            href="mailto: samueladebayoopeyemi@gmail.com"
            target="_blank"
            rel="noreferrer"
          >
            <AiFillMail className="icon-size" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
