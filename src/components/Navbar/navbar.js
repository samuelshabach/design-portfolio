import "../Navbar/navbar.css";
import hamburger from "../../assets/images/hamburger.png";
import { NavLink } from "react-router-dom";
import * as Scroll from "react-scroll";

const Navbar = () => {
  let Link = Scroll.Link;
  return (
    <nav className="navbar navbar-expand-lg navbar-light container mt-3 mt-md-4">
      <NavLink
        className="navbar-brand text-font-weight mt-md-1 nav-font"
        to="/"
      >
        ✌️
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <img src={hamburger} alt="hamburger" className="hamburger" />
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-md-auto">
          <li className="nav-item mr-md-2 mt-3 mt-md-2 dropdown">
            <span
              className="navigation-link mr-md-3"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Projects
            </span>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <NavLink
                className="navigation-link-drop dropdown-item"
                to="/designs"
                activeStyle={{
                  fontWeight: "semibold",
                  backgroundColor: "#FFEBB4",
                  padding: 12,
                  color: "#000000",
                }}
              >
                Design
              </NavLink>
              <div className="dropdown-divider"></div>
              <NavLink
                className="navigation-link-drop dropdown-item"
                to="/development"
                activeStyle={{
                  fontWeight: "semibold",
                  backgroundColor: "#FFEBB4",
                  padding: 12,
                  color: "#000000",
                }}
              >
                Development
              </NavLink>
            </div>
          </li>
          <li className="nav-item mr-md-2 mt-4 mt-md-2">
            <NavLink
              className="navigation-link mr-md-3"
              to="/learning"
              activeStyle={{
                fontWeight: "semibold",
                backgroundColor: "#FFEBB4",
                padding: 12,
                borderRadius: 5,
              }}
            >
              Learning
            </NavLink>
          </li>
          <li className="nav-item mt-4 mt-md-2">
            <Link
              to="footer"
              spy={true}
              smooth={true}
              duration={500}
              className="navigation-link some-active-class mr-2 mr-md-3"
            >
              Contact
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};
export default Navbar;
