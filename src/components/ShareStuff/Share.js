import React from "react";
import "../ShareStuff/Share.css";
import {
  AiFillFacebook,
  AiFillTwitterSquare,
  AiOutlineLink,
  AiOutlineWhatsApp,
} from "react-icons/ai";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class ShareStuff extends React.Component {
  state = {
    value: "",
    copied: false,
  };
  render() {
    let urlToShare = window.location;
    const notify = () =>
      toast.dark("Copied", {
        position: "bottom-right",
        autoClose: 2000,
      });

    return (
      <div className="container">
        <div className="d-flex">
          <div>
            <p className="share-text">🤝 Share</p>
          </div>
          <div className="ml-auto">
            {/* copy link */}
            <CopyToClipboard
              text={urlToShare}
              onCopy={() => this.setState({ copied: true })}
            >
              <span className="span-link" onClick={notify}>
                <AiOutlineLink className="share-icon-size" />
              </span>
            </CopyToClipboard>

            {/* Facebook */}
            <a
              className="px-3 mb-2"
              href={`https://www.facebook.com/sharer/sharer.php?u=${urlToShare}`}
              target="_blank"
              rel="noreferrer"
            >
              <AiFillFacebook className="share-icon-size" />
            </a>

            {/* twitter */}
            <a
              className=""
              href={`http://twitter.com/share?text=Checkout this blog post&url=${urlToShare}`}
              target="_blank"
              rel="noreferrer"
            >
              <AiFillTwitterSquare className="share-icon-size" />
            </a>

            {/* whatsapp */}
            <a
              className="px-3 mb-2"
              href={`http://wa.me/?text=Checkout this blog post&url=${urlToShare}`}
              target="_blank"
              rel="noreferrer"
            >
              <AiOutlineWhatsApp className="share-icon-size" />
            </a>
          </div>
        </div>
        {this.state.copied ? (
          <ToastContainer style={{ textDecorationColor: "black" }} />
        ) : null}
      </div>
    );
  }
}

export default ShareStuff;
