<div className="mt-5 mt-md-5 px-md-5">
  <Link to="/projects/geek" className="project-link">
    <div className="card main-card" style={{ backgroundColor: "black" }}>
      <div className="card-body">
        <div className="d-flex flex-column flex-md-row">
          <div className="mt-2 mt-md-5 px-md-5 project-head">
            <h1 className="text-white mt-2 mt-md-5 px-md-5 text-center text-md-left">
              Geeks
            </h1>
            <p className="text-white px-md-5 text-center text-md-left">
              A Broker app for all trades and platform
            </p>
          </div>

          {/* image */}
          <div className="m-auto ml-md-auto px-md-5">
            <img
              className=""
              src={frame1}
              height={300}
              width={150}
              alt="iphone frame"
            />
          </div>
        </div>
      </div>
    </div>
  </Link>
</div>;
