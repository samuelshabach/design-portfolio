import "../DevList/devlist.css";
import { HiOutlineExternalLink } from "react-icons/hi";

const DevList = () => {
  return (
    <div className="container">
      {/* heading */}
      <div className="mt-5 mt-md-4 container">
        <h4 className="project-name text-center geek-head">
          UI development projects
        </h4>
      </div>

      <div className="d-flex w-100 mt-4">
        {/* geeks ui */}
        <a
          href="https://bitbucket.org/samuelshabach/geeks_mobile/src/master/"
          className="w-40 dev-link-style"
          target="_blank"
          rel="noreferrer"
        >
          <div class="card bg-white mt-4 py-2 devCard">
            <div class="card-body">
              <div className="d-flex">
                <h5
                  class="card-title"
                  style={{ fontSize: 18, fontWeight: 600, color: "black" }}
                >
                  Geeks - A mobile app for trading
                </h5>
                <HiOutlineExternalLink className="ml-auto" />
              </div>

              <p class="card-text" style={{ color: "black" }}>
                The UI was done with React Native
              </p>
              <div className="d-flex">
                <span className="tag1 px-2 py-1">Mobile</span>
                <span className="tag1 px-2 py-1 mx-3">React Native</span>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
};

export default DevList;
