import "../Lessons_WW1/Lessons.css";
import Footer from "../../../components/Footer/Footer";
import ShareStuff from "../../../components/ShareStuff/Share";
import IMG2 from "../../../assets/images/IMG2.png";
import ScrollUpButton from "react-scroll-up-button";
import * as Scroll from "react-scroll";

const LessonsWW1 = () => {
  let Element = Scroll.Element;
  return (
    <div className="container mt-5">
      {/*  */}
      <div>
        <p className="text-center">
          <span className="tag2 px-2 py-1">History</span>
        </p>
        <h2 className="text-center lesson-head">Lessons from World war 1</h2>
        <p className="date text-center">18th, June, 2021</p>
      </div>
      {/*  */}
      <div className="d-flex flex-column mt-2">
        <div className="m-auto w-75 img-contain">
          <img className="w-100" src={IMG2} alt="lessons_from_WW1" />
        </div>
      </div>

      {/*  */}
      <div className="body-contain">
        {/* quote */}
        <div className="mt-5">
          <div className="lesson-quote-contain">
            <blockquote className="blockquote text-center">
              <p class="mb-0">
                If you could hear, at every jolt, the blood <br />
                Come gargling from the froth-corrupted lungs,
                <br />
                Obscene as cancer, bitter as the cud
                <br />
                Of vile, incurable sores on innocent tongues,—
                <br />
                My friend, you would not tell with such high zest
                <br />
                To children ardent for some desperate glory,
                <br />
                The old Lie: Dulce et decorum est
                <br />
                Pro patria mori [Latin for “Sweet and fitting it is to die for
                one’s country”].
              </p>
              <footer className="blockquote-footer">
                Wilfred Owen <br /> <i>British poet who fought in the war</i>
              </footer>
            </blockquote>
          </div>
        </div>
        <p className="mt-4">
          Well, I decided to go through with this because as much as we don't
          want to admit, World War 1 happened and it altered the essence of life
          itself. It was once called the Great War, the most cataclysmic single
          war in Western history ever since the fall of Rome and one of the
          worst and most lethal in world history.
        </p>
        <p>
          I am of the opinion that the war was a result of political egotism,
          accompanied by illogical and rash decisions. Most of the nations
          involved in the war had more reason not to fight than to fight. Even
          when everyone was losing so much, and gaining almost nothing but death
          and destruction, they persisted in conducting the war.
        </p>
        <p>History is not just to be known, it is to be learnt from.</p>
        {/*  */}
        <div>
          <h5 style={{ fontWeight: 700 }}>Here are a few lessons</h5>

          {/* list of lessons */}
          <ul>
            <li className="li-contain">
              <strong>War is possible no matter how great things seem.</strong>
              <br />
              The world before the war seemed calm with countries like Germany,
              France, Great Britain, and Austria-Hungary which represented the
              most advanced civilizations technologically, scientifically,
              culturally then were considerably at peace with one another. They
              all had intimate ties with each other, both between individual
              leaders and as empires and nations as a whole, ties that bound
              them culturally, economically, socially, and politically.
              <br />
              Europe was said to have seen a long stretch of peace, there were
              no wars on the European continent from the{" "}
              <a
                href="https://en.wikipedia.org/wiki/Battle_of_Waterloo"
                className="lesson-link"
                target="_blank"
                rel="noreferrer"
              >
                final defeat of Napoleon at Waterloo in 1815
              </a>{" "}
              to the outbreak of World War I in 1914.
              <br />
              <strong>
                It is worthy to note that none of these mattered. The war still
                broke out.
              </strong>
            </li>
            <li className="mt-4 li-contain">
              <strong>
                Human behaviour can be erratic and this can come with
                consequences.
              </strong>
              <br />
              The war is a proof of the egostic nature of man which can cause
              him to make rash decisions. Countries which were losing still
              continued based on instructions from leaders.
            </li>
            <li className="mt-4 li-contain">
              <strong>
                Decisions of war and peace are up to us and only us, and we own
                the results.
              </strong>
              <br />
              {/* quote */}
              <div className="">
                <div className="lesson-quote-contain">
                  <blockquote className="blockquote text-center">
                    <p class="mb-0">
                      The First World War was a tragic and unnecessary conflict.
                    </p>
                    <footer className="blockquote-footer">
                      John Keegan <br />
                      <i>The First World War</i>
                    </footer>
                  </blockquote>
                </div>
              </div>
              Not everything that happened was for a reason. Like I said, I
              still stand by my opinion of why the war came to be. The stupidity
              of the rash decisions made led to the global war and its
              perpetuation and this shows how utterly avoidable and unnecessary
              the overall conflict was.
              <br />
              It was recorded that the total number of military and civilian
              casualties in World War I, was around 40 million. There were 20
              million deaths and 21 million wounded. The total number of deaths
              included 9.7 million military personnel and about 10 million
              civilians.
              <br />
              <strong>
                Humanity owns the results of its decisions, no need blaming a
                "Supreme Being".
              </strong>
            </li>
          </ul>
        </div>

        {/* share */}
        <hr />
        <ShareStuff />

        <hr className="bottom-line" />
        {/* connect */}
        <p className="text-center" style={{ lineHeight: 2 }}>
          <strong>Let's connect</strong> <br />
          Kindly stalk for opportunities or to say hi! 👋
        </p>

        <Element name="footer">
          <Footer />
        </Element>
      </div>

      {/* scroll up */}
      <div>
        <ScrollUpButton />
      </div>
    </div>
  );
};

export default LessonsWW1;
