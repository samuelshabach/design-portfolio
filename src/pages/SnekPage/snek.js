import "../SnekPage/snek.css";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer/Footer";
import Desktop6 from "../../assets/images/Snek/Desktop6.png";
import DoubleDiamond from "../../assets/images/Snek/DoubleDiamond.png";
import ScrollUpButton from "react-scroll-up-button";
import * as Scroll from "react-scroll";

const SnekPage = () => {
  let Element = Scroll.Element;
  return (
    <div className="">
      {/* introduction text and picture */}
      <h3 className="text-center mt-5 mt-md-4 geek-head">Snek</h3>
      {/*  */}
      <div className="d-flex flex-column mt-5">
        <div className="m-auto w-100 img-contain">
          <img className="w-100" src={Desktop6} alt="snek front pic" />
        </div>
      </div>

      {/* body */}
      <div className="container">
        {/* Role and Team */}
        <div className="snek-margin">
          <div className="card mt-5">
            <div className="card-body">
              <div className="d-flex flex-column flex-md-row">
                {/* Role */}
                <div>
                  <h5 className="geek-head text-center">Role</h5>
                  <p className="text-center snek-p">
                    UI/UX designer <br /> User research <br /> Project lead
                  </p>
                </div>

                {/* Team */}
                <div className="ml-0 ml-md-auto mt-4 mt-md-0">
                  <h5 className="geek-head text-center">Team</h5>
                  <p className="text-center snek-p">
                    Samuel Adebayo <br /> (Solo)
                  </p>
                </div>

                {/* Duration */}
                <div className="ml-0 ml-md-auto mt-4 mt-md-0">
                  <h5 className="geek-head text-center">Duration</h5>
                  <p className="text-center snek-p">
                    January 2021 <br /> (approx. 5 months)
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Overview */}
        <div className="snek-margin">
          <h5 className="geek-head snek-margin">Overview</h5>
          <hr />
          <p className="mt-4">
            Snek is an e-commerce mobile app created for a startup in South
            Africa. It is both for sales and manufacture of footwears to be
            distributed in the primary marketplace(Africa).
          </p>
          <p className="mt-1">
            It was aimed to provide quality footwears for buyers as well as
            entreprenuerial assistance for local manufacturers.
          </p>
          <p className="mt-1">
            This was both a review and an improvement of pre-existing
            application.
          </p>
        </div>

        {/* Understanding the problem */}
        <div className="snek-margin snek-contain">
          <h5 className="geek-head">Problems</h5>
          <hr />
          <p className="mt-4">
            User research is an important aspect of a product. Thus, the
            opinions of users are such that cannot be overlooked.
          </p>
          <p className="mt-1">
            The following were problems faced by people(users):
          </p>
          <ul>
            <li className="li-contain">
              Screens congested with many information, which can be very
              confusing.
            </li>
            <li className="mt-4 li-contain">
              Inability to make preferences, filter options.
            </li>
            <li className="mt-4 li-contain">
              Difficulties in cart screen, payment methods.
            </li>
          </ul>
        </div>

        {/* Key insights */}
        <div className="snek-margin">
          <h5 className="geek-head snek-margin">Key insights</h5>
          <hr />
          <p className="mt-4">
            Here are key points noted from the interviews and surveys conducted
            on pre-existing users and target audience/market:
          </p>
          <div className="d-flex flex-column flex-md-row mt-4">
            {/* first card */}
            <div className="shadow-sm w-50 card-style">
              <div className="card-body">
                <p>
                  There is need for a simple interface that simplifies the
                  experience.
                </p>
              </div>
            </div>

            {/* second card */}
            <div className="shadow-sm mx-md-3 mt-4 mt-md-0 w-50 card-style">
              <div className="card-body">
                <p>
                  Users find it hard navigating the application with frustration
                  setting in as a result.
                </p>
              </div>
            </div>

            {/* third card */}
            <div className="shadow-sm ml-md-auto mt-4 mt-md-0 w-50 card-style">
              <div className="card-body">
                <p>
                  Most users showed interest in a minimalistic interface with
                  easy access to information than something colourful but
                  confusing.
                </p>
              </div>
            </div>
          </div>
        </div>

        {/* Ideation */}
        <div className="snek-margin snek-contain">
          <h5 className="geek-head">Ideation</h5>
          <hr />
          <p className="mt-4">
            There were many things that a solution could have, but I decided
            these were the most pressing.
          </p>
          <ul>
            <li className="li-contain">Simplicity should get the job done.</li>
            <li className="mt-4 li-contain">
              A filter option is very needed. It helps deliver the needed
              information as needed by a user.
            </li>
            <li className="mt-4 li-contain">
              No matter how simple a solution should look, it must also be
              appealing.
            </li>
          </ul>
        </div>

        {/* Solution implementation */}
        <div className="snek-margin">
          <h5 className="geek-head snek-margin">Implementation</h5>
          <hr />
          <p className="mt-4">
            During series of sketching and prototyping, I implemented the
            following
          </p>
          <ul>
            <li className="li-contain">
              <strong>Established a design system.</strong> This was important
              as it would assist the Engineering team understand how and why
              certain components were chosen over others.
            </li>
            <li className="li-contain mt-4">
              <strong>Established a design model.</strong> I implemented the{" "}
              <strong>Double Diamond</strong>. This was important as it helped
              in having a consistent design flow and process. It also made
              iteration systematic.
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img
                    className="w-100"
                    src={DoubleDiamond}
                    alt="design model"
                  />
                </div>
              </div>
            </li>
            <li className="li-contain mt-4">
              <strong>Utilized some design trends.</strong> This helped in
              solving some of the interface problems faced by users. I made use
              of <strong>Minimalism</strong> and <strong>Simplified UX</strong>{" "}
              type of design.
            </li>
          </ul>
          <p className="mt-4">
            Due to contract terms and client-product privileges, I would only be
            sharing some prototypes.
          </p>
          {/* button */}
          <div className="mt-4 mt-md-4">
            <a
              href="https://www.figma.com/proto/LvHH2tUA7E4qbKYlk8t47y/Snek-Online-sneakers-store?node-id=607%3A117&scaling=scale-down&page-id=548%3A13&starting-point-node-id=607%3A117"
              target="_blank"
              rel="noreferrer"
              className="btn btn-md-block snek-btn text-white"
              style={{ fontWeight: 500, fontSize: 14, width: 150 }}
            >
              Check prototype
            </a>
          </div>
        </div>

        {/* Results and Takeaways */}
        <div className="snek-margin snek-contain">
          <h5 className="geek-head">Results and Takeaways</h5>
          <hr />
          <p className="mt-4">Some key takeaways from this project are:</p>
          {/* lists of highlights */}
          <ul>
            <li className="li-contain">
              <strong>Simplicity is the goal.</strong> Complex problems are to
              be made simple enough for the users. Access to information is very
              vital and this must come to the user as easy and simple as
              possible.
            </li>
            <li className="mt-4 li-contain">
              <strong>Solving a problem is not enough.</strong> It's important
              to ensure that solutions are provided with the best methods in
              place. Don't just look to solve anyhow, solve problems
              articulately.
            </li>
          </ul>
        </div>

        {/* more projects */}
        <hr />
        {/* button */}
        <div className="d-flex mt-2 w-100">
          <div className="m-auto">
            <Link to="/designs">
              <button
                className="btn btn-md-block more text-dark"
                style={{ fontWeight: 600, fontSize: 14, width: 200 }}
              >
                View more projects
              </button>
            </Link>
          </div>
        </div>

        {/* scroll up */}
        <div>
          <ScrollUpButton />
        </div>
        <hr />
        {/* connect */}
        <p className="text-center" style={{ lineHeight: 2 }}>
          <strong>Let's connect</strong> <br />
          Kindly stalk for opportunities or to say hi! 👋
        </p>
      </div>

      <Element name="footer">
        <Footer />
      </Element>
    </div>
  );
};

export default SnekPage;
