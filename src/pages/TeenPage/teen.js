import "../TeenPage/teen.css";
import Footer from "../../components/Footer/Footer";
import Home from "../../assets/images/Teens/Home.png";
import SignUp from "../../assets/images/Teens/SignUp.png";
import Onboarding from "../../assets/images/Teens/Onboarding.png";
import Messages from "../../assets/images/Teens/Messages.png";
import Dashboard from "../../assets/images/Teens/Dashboard.png";
import Settings from "../../assets/images/Teens/Settings.png";
import Personas2 from "../../assets/images/Personas2.png";
import Desktop5 from "../../assets/images/Desktop5.png";
import { Link } from "react-router-dom";
import ScrollUpButton from "react-scroll-up-button";
import * as Scroll from "react-scroll";

const TeenPage = () => {
  let Element = Scroll.Element;
  return (
    <div>
      {/* header */}
      <h3 className="text-center mt-5 mt-md-4 geek-head">Teenlearner</h3>
      {/*  */}
      <div className="d-flex flex-column mt-5">
        <div className="m-auto w-100 img-contain">
          <img className="w-100" src={Desktop5} alt="teen front page" />
        </div>
      </div>
      <div className="container">
        <div className="d-flex flex-column flex-md-row">
          {/* Overview */}
          <div className="">
            <h5 className="mt-5">Overview</h5>
            <p className="mt-4">
              Teenlearner is a learning platform geared to helping get schooled
              from home. <br /> There is only a web version for now. It was
              created to help learning during <br /> the lockdown due to the
              COVID issue around the globe.
            </p>
          </div>

          {/* Role */}
          <div className="ml-md-auto">
            <h5 className="mt-5">Role</h5>
            <p className="mt-4">
              Product designer, User research, <br /> User interaction
            </p>
          </div>
        </div>

        {/* Background */}

        <div className="mt-5">
          <h3 className="geek-head background-head">Background</h3>
          <p className="mt-4">
            COVID was a life-changing occurrence for the world and as things
            happened, the world in itself had to adjust to the life and changes
            that came upon it. This brought about an home schooling idea for
            most part of the world.
          </p>
          <h3 className="mt-4 text-center">
            <strong>I was the sole designer on the team</strong>
          </h3>
          <p className="mt-4">
            Here are a few highlights of what I implemented:
          </p>
          {/* lists of highlights */}
          <ul>
            <li className="li-contain">
              <strong>Assisted in turning an idea into a product.</strong> The
              ability to transform an idea into something much more tangible is
              a process that is both exhilarating and rewarding. I worked
              closely with the team of five to shape the product vision and
              strategy. While the product is still in development, being able to
              see how much we have grown as a team is wonderful.
            </li>
            <li className="mt-4 li-contain">
              <strong>Established a design system.</strong> This was important
              as it would assist the Engineering team understand how and why
              certain components were chosen over others.
            </li>
          </ul>
        </div>

        {/* Problem identification */}
        <div className="background-head">
          {/* Problem */}
          <div className="div-contain">
            <h3 className="geek-head">Understanding the problem</h3>
            <p className="mt-4">
              As COVID would have it, the world was in lockdown for months,
              physical contact and most especially learning was restricted.
            </p>
            <p className="mt-0">The following were problems/pains faced:</p>

            {/* lists of problems */}
            <ul className="li-contain">
              <li>Difficulty in home-schooling without supervision.</li>
              <li className="mt-4 li-contain">
                No streamlined learning path based on interests.
              </li>
              <li className="mt-4 li-contain">
                Inability to collaborate real-time with other students taking a
                particular course.
              </li>
            </ul>

            {/* user personas */}
            <div>
              <h3 className="geek-head background-head">User personas</h3>
              <p className="mt-4">
                Based on our research, we recognised that there were 2 key user
                types that the product tried to solve problems for.
              </p>

              {/*  */}
              <div className="d-flex flex-column mt-5">
                <div className="m-auto w-100 img-contain">
                  <img className="w-100" src={Personas2} alt="personas" />
                </div>
              </div>
            </div>
          </div>

          {/* Solutions */}
          <div>
            <h3 className="geek-head background-head">Product vision</h3>
            <p className="mt-4">
              The following were areas the product aimed to focus on:
            </p>

            {/* lists of solutions */}
            <ul>
              <li className="li-contain">
                Bridge the gap of learning at and from home.
              </li>
              <li className="mt-4 li-contain">
                Establish a supervising analytics system that help students
                focus and also help guardians with supervision.
              </li>
              <li className="mt-4 li-contain">
                Provide a collaboration-based system to encourage participation,
                collaboration and synchronization among students.
              </li>
            </ul>
            <p>
              Aside from being the Product designer, I was also into developing
              the UI using React.{" "}
              <Link to="/development" className="teen-link">
                Check out the development page
              </Link>
            </p>
          </div>

          {/* MVP */}
          <div className="div-contain">
            <h3 className="geek-head">Defining the MVP</h3>
            <p className="mt-4">
              I did some sketching, user flow to capture the MVP.
            </p>
            <p>The following were highlighted as key features:</p>
            {/* lists of highlights */}
            <ul>
              <li className="li-contain">
                Allow users pick from a list of courses based on interest.
              </li>
              <li className="mt-4 li-contain">
                Messaging/Forum system for collaboration amongst students and
                also between student and teacher.
              </li>
              <li className="mt-4 li-contain">
                Analytic system to help gauge participation.
              </li>
            </ul>

            {/* screen list */}
            <p className="mt-4">
              I mocked up the main pages for the MVP based off the sketches
              created and came up with the following:
            </p>
            <ul>
              <li>Home page</li>
              <li className="mt-3">Sign up</li>
              <li className="mt-3">Onboarding question</li>
              <li className="mt-3">Courses</li>
              <li className="mt-3">Dashboard</li>
              <li className="mt-3">Messaging</li>
              <li className="mt-3">Stats</li>
              <li className="mt-3">Settings</li>
            </ul>
          </div>

          {/* Designs */}
          <div className="background-head">
            <div>
              <h3 className="geek-head">Designs</h3>
              <p className="mt-4">
                I created some basic designs from the sketches for the MVP which
                included some solutions we would have the product provide.
              </p>
            </div>

            {/* Home page */}
            <div>
              <p className="mt-4">
                <strong>Home page</strong> <br />
                This shows a brief introduction and a 'video button' that gives
                an overview of what the product is all about.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={Home} alt="home" />
                </div>
              </div>
            </div>

            {/* Sign up */}
            <div className="design-head">
              <p className="mt-5">
                <strong>Sign up</strong> <br />
                This is simply for a quick but comprehensive sign up.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={SignUp} alt="sign up" />
                </div>
              </div>
            </div>

            {/* Onboarding */}
            <div className="design-head">
              <p className="mt-5">
                <strong>Onboarding</strong> <br />A set of pages with questions
                to help understand the interests of students/users.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={Onboarding} alt="onboarding" />
                </div>
              </div>
            </div>

            {/* Dashboard */}
            <div className="design-head">
              <p className="mt-5">
                <strong>Dashboard</strong> <br />
                Here, we have the basic components for collaboration, follow up
                and other vital information vital and unique to each students.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={Dashboard} alt="dashboard" />
                </div>
              </div>
            </div>

            {/* Messages */}
            <div className="design-head">
              <p className="mt-5">
                <strong>Messages</strong> <br />A simple chat-like messaging
                system to aid collaboration.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={Messages} alt="messages" />
                </div>
              </div>
            </div>

            {/* Settings */}
            <div className="design-head">
              <p className="mt-5">
                <strong>Settings</strong> <br />
                This is to let students/users edit information and also set
                preferences.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-75 img-contain">
                  <img className="w-100" src={Settings} alt="settings" />
                </div>
              </div>
            </div>
          </div>

          {/* Results and takeaway */}
          <div className="background-head">
            <div className="div-contain">
              <h3 className="geek-head">Results and Takeaways</h3>
              <p className="mt-4">
                I learnt a lot from this project most especially how a project
                is not as straightforward as it seems.
              </p>
              <p>Some key takeaways from this project are:</p>
              {/* lists of highlights */}
              <ul>
                <li className="li-contain">
                  <strong>Be flexible.</strong> Looking at the things done, I
                  saw how flexibility is key in designing. There would be need
                  for tradeoffs; Therefore rigidity is not a strong point in
                  designing.
                </li>
                <li className="mt-4 li-contain">
                  <strong>There would be iteration.</strong> It's important to
                  understand this from the beginning. The first prototype might
                  not be the one to make the market, hence it is vital to be
                  open to criticism and collaboration from and with
                  non-designers.
                </li>
              </ul>
            </div>
          </div>
        </div>

        {/* more projects */}
        <hr />
        {/* button */}
        <div className="d-flex mt-2 w-100">
          <div className="m-auto">
            <Link to="/designs">
              <button
                className="btn btn-md-block more text-dark"
                style={{ fontWeight: 600, fontSize: 14, width: 200 }}
              >
                View more projects
              </button>
            </Link>
          </div>
        </div>

        {/* scroll up */}
        <div>
          <ScrollUpButton />
        </div>
        <hr />
        {/* connect */}
        <p className="text-center" style={{ lineHeight: 2 }}>
          <strong>Let's connect</strong> <br />
          Kindly stalk for opportunities or to say hi! 👋
        </p>
      </div>

      <Element name="footer">
        <Footer />
      </Element>
    </div>
  );
};

export default TeenPage;
