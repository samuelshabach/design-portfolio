import "../geek-page/geek.css";
import Personas1 from "../../assets/images/Personas1.png";
import Desktop4 from "../../assets/images/Desktop4.png";
import splash from "../../assets/images/splash.png";
import SignUp from "../../assets/images/SignUp.png";
import Verification from "../../assets/images/Verification.png";
import SignIn from "../../assets/images/SignIn.png";
import Onboard from "../../assets/images/Onboard.png";
import Home from "../../assets/images/Home.png";
import Footer from "../../components/Footer/Footer";
import { Link } from "react-router-dom";
import ScrollUpButton from "react-scroll-up-button";
import * as Scroll from "react-scroll";

const GeekPage = () => {
  let Element = Scroll.Element;
  return (
    <div>
      {/* header */}
      <h3 className="text-center mt-5 mt-md-4 geek-head">Geeks</h3>
      {/*  */}
      <div className="d-flex flex-column mt-5">
        <div className="m-auto w-100 img-contain">
          <img className="w-100" src={Desktop4} alt="geek front pic" />
        </div>
      </div>
      <div className="container">
        <div className="d-flex flex-column flex-md-row">
          {/* Overview */}
          <div className="">
            <h5 className="mt-5">Overview</h5>
            <p className="mt-4">
              Geeks is a broker platform still in production. This is the mobile
              version design. <br />
              It was created in response to issues and problems experienced by
              seasoned traders and users.
            </p>

            {/* button */}
            <div className="mt-4 mt-md-4">
              <a
                href="https://www.figma.com/proto/WOlGDBMilVAFT8GCWw4TSJ/Geeks-Broker?node-id=8%3A5&scaling=contain&page-id=0%3A1"
                target="_blank"
                rel="noreferrer"
                className="btn btn-md-block geek-btn text-white"
                style={{ fontWeight: 500, fontSize: 14, width: 150 }}
              >
                Check prototype
              </a>
            </div>
          </div>

          {/* Role */}
          <div className="ml-md-auto">
            <h5 className="mt-5">Role</h5>
            <p className="mt-4">
              Product designer, User research <br /> User interaction, UI
              development
            </p>
          </div>
        </div>

        {/* Background */}

        <div className="mt-5">
          <h3 className="geek-head background-head">Background</h3>
          <p className="mt-4">
            I was a big fan of trading; I still am. I had quite some accounts on
            different broking platform. I had friends that I also introduced. My
            friends began talking about issues they all had, which I had
            noticed. So I decided to research and provide a solution. <br />I
            was the sole designer and I was responsible for the product design
            and UX/UI experience for the app.
          </p>
          <p>Here is a highlight of what I implemented:</p>
          {/* lists of highlights */}
          <ul>
            <li className="li-contain">
              <strong>Established a design system.</strong> This was important
              as it would assist the Engineering team understand how and why
              certain components were chosen over others.
            </li>
          </ul>
        </div>

        {/* Problem identification */}
        <div className="background-head">
          {/* quote */}
          <div className="quote-contain">
            <blockquote className="blockquote text-center">
              <p class="mb-0">A problem well stated is a problem half solved</p>
              <footer className="blockquote-footer">John Dewey</footer>
            </blockquote>
          </div>

          {/* Problem */}
          <div>
            <h3 className="geek-head background-head">
              Understanding the problem
            </h3>
            <p className="mt-4">
              The following were problems/pains faced by traders/users:
            </p>

            {/* lists of problems */}
            <ul>
              <li className="li-contain">
                Inability of users to have access to different types of trades
                like currencies, cypto, indices via the same sub-account.
              </li>
              <li className="mt-4 li-contain">
                Inability to add and use multiple accounts on one app.
              </li>
              <li className="mt-4 li-contain">
                Inability to speculate and get real-time notification on a
                particular trade.
              </li>
            </ul>

            {/* user personas */}
            <div>
              <h3 className="geek-head background-head">User personas</h3>
              <p className="mt-4">
                Based on our research, we recognised that there were 2 key user
                types that the product tried to solve problems for.
              </p>

              {/*  */}
              <div className="d-flex flex-column mt-5">
                <div className="m-auto w-100 img-contain">
                  <img className="w-100" src={Personas1} alt="personas" />
                </div>
              </div>
            </div>

            {/* quote 2 */}
            <div className="background-head">
              <div className="quote-contain">
                <blockquote className="blockquote text-center">
                  <p class="mb-0">
                    There's no use talking about the problem unless you talk
                    about the solution.
                  </p>
                  <footer className="blockquote-footer">Betty Williams</footer>
                </blockquote>
              </div>
            </div>
          </div>

          {/* Solutions */}
          <div>
            <h3 className="geek-head background-head">
              Product vision and solution
            </h3>
            <p className="mt-4">
              From these findings, we decided as a product to identify key
              business goals:
            </p>

            {/* lists of solutions */}
            <ul>
              <li className="li-contain">
                We want users to be able to trade different types of commodities
                like currencies, cypto, indices via the same sub-account.
              </li>
              <li className="mt-4 li-contain">
                We want users to be able to add and use multiple accounts to one
                app.
              </li>
              <li className="mt-4 li-contain">
                We want users to be able to to speculate and get real-time
                notification on a particular trade based on their preferences.
              </li>
            </ul>
            <p>
              As a starting point, we each did some market research on
              competitors to investigate the current offerings in the market and
              take inspiration from particular features that we liked about each
              app.
            </p>
          </div>

          {/* MVP definition */}
          <div className="background-head">
            <div className="mvp-contain">
              <h3 className="geek-head">Defining the MVP</h3>
              <p className="mt-4">
                I did some sketching, user flow to capture the MVP.
              </p>
              <p>The following were highlighted as key features:</p>
              {/* lists of highlights */}
              <ul>
                <li className="li-contain">
                  <strong>Goal definition.</strong> Allow users add multiple
                  accounts, trade different commodities and get real-time
                  notification.
                </li>
                <li className="mt-4 li-contain">
                  <strong>Inbuilt commodities system.</strong> Users have most
                  commodities displayed and can also add from a list of
                  commodities available.
                </li>
                <li className="mt-4 li-contain">
                  <strong>Visible notification alerts.</strong> Users receive
                  notifications and can access them easily from the home screen.
                </li>
              </ul>

              {/* screen list */}
              <p className="mt-4">
                I mocked up the main screens for the MVP based off the sketches
                created and came up with the following:
              </p>
              <ul>
                <li>Splash screen</li>
                <li className="mt-3">Sign up</li>
                <li className="mt-3">Verify mail</li>
                <li className="mt-3">Onboarding question</li>
                <li className="mt-3">Sign in</li>
                <li className="mt-3">Home screen</li>
                <li className="mt-3">Settings</li>
              </ul>
            </div>
          </div>

          {/* Designs */}
          <div className="background-head">
            <div>
              <h3 className="geek-head">Designs</h3>
              <p className="mt-4">
                I created some basic designs from the sketches for the MVP which
                included some solutions we would have the product solve.
              </p>
            </div>

            {/* splash */}
            <div>
              <p className="mt-4">
                <strong>Splash/Get started screen</strong> <br />
                This just displays the logo and a brief info as regards the
                product.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img className="w-100" src={splash} alt="splash" />
                </div>
              </div>
            </div>

            {/* sign up */}
            <div>
              <p className="mt-4">
                <strong>Sign up</strong> <br />
                This is a simple sign up form with the most important details
                required.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img className="w-100" src={SignUp} alt="sign up" />
                </div>
              </div>
            </div>

            {/* Verification */}
            <div>
              <p className="mt-4">
                <strong>Verification</strong> <br />
                This is for security reasons and it is required.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img
                    className="w-100"
                    src={Verification}
                    alt="verification"
                  />
                </div>
              </div>
            </div>

            {/* Onboarding */}
            <div>
              <p className="mt-4">
                <strong>Onboarding</strong> <br />
                This helps in configuring type of account and currencies users
                would like to engage in trading.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img className="w-100" src={Onboard} alt="onboarding" />
                </div>
              </div>
            </div>

            {/* Sign in */}
            <div>
              <p className="mt-4">
                <strong>Sign in</strong> <br />
                This is a simple sign in to enable existing users have access to
                their accounts.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img className="w-100" src={SignIn} alt="sign in" />
                </div>
              </div>
            </div>

            {/* Home */}
            <div>
              <p className="mt-4">
                <strong>Home screen</strong> <br />
                This is where a highlight of commodities info is seen alongside
                notifications from preferences and news.
              </p>
              <div className="d-flex flex-column mt-2">
                <div className="m-auto w-25 img-contain">
                  <img className="w-100" src={Home} alt="home" />
                </div>
              </div>
            </div>
          </div>

          {/* Results and takeaway */}
          <div className="background-head">
            <div className="mvp-contain">
              <h3 className="geek-head">Results and Takeaways</h3>
              <p className="mt-4">
                I learnt a lot from this project most especially where to focus
                my energy and efforts.
              </p>
              <p>Some key takeaways from this project are:</p>
              {/* lists of highlights */}
              <ul>
                <li className="li-contain">
                  <strong>Focus on the problem.</strong> One thing that must
                  never be forgotten is the fact that you are passionate about
                  solving user pains. At the end of the day, that is all that
                  matters.
                </li>
                <li className="mt-4 li-contain">
                  <strong>Focus on building an MVP.</strong> It's important to
                  focus on the features that can deliver the highest value for
                  your users. Ensure not to go outside of the scope of the
                  problems intended to be solved, with this you can maximize
                  your time and energy on the most important features that can
                  get the job done for the users.
                </li>
              </ul>
            </div>
          </div>
        </div>

        {/* more projects */}
        <hr />
        {/* button */}
        <div className="d-flex mt-2 w-100">
          <div className="m-auto">
            <Link to="/designs">
              <button
                className="btn btn-md-block more text-dark"
                style={{ fontWeight: 600, fontSize: 14, width: 200 }}
              >
                View more projects
              </button>
            </Link>
          </div>
        </div>

        {/* scroll up */}
        <div>
          <ScrollUpButton />
        </div>
        <hr />
        {/* connect */}
        <p className="text-center" style={{ lineHeight: 2 }}>
          <strong>Let's connect</strong> <br />
          Kindly stalk for opportunities or to say hi! 👋
        </p>
      </div>

      <Element name="footer">
        <Footer />
      </Element>
    </div>
  );
};

export default GeekPage;
