import { Link } from "react-router-dom";
import "../homepage/homepage.css";
import Footer from "../../components/Footer/Footer";
import * as Scroll from "react-scroll";

const Homepage = () => {
  let Element = Scroll.Element;
  return (
    <div className="container mt-2 mt-md-2">
      <div className="d-flex">
        {/* left side */}
        <div className="w-100 py-5 px-2 px-md-3">
          <div className="mt-0 mt-md-5">
            <h3 className="my-name">Hello👋, I'm Samuel Adebayo</h3>
            <h5 className="product">
              I{" "}
              <Link to="/designs" className="home-link1">
                design
              </Link>
              <span> and </span>{" "}
              <Link className="home-link2" to="/development">
                develop
              </Link>
            </h5>
            <p className="mt-4">
              I am a product designer + ui developer with the passion for
              conceptualizing and delivering elegant, user friendly and
              efficient software products designs with a good understanding of
              programming and business principles. I love solving problems with
              design thus improving the lives of others in the process.
            </p>
            <p className="mt-3">
              I love playing basketball. LeBron James is my favourite
              basketballer, Cristiano Ronaldo is my favourite footballer ❤️
            </p>

            {/* button */}
            <div className="mt-4 mt-md-4 dropdown">
              <button
                className="btn btn-md-block my-btn text-white"
                style={{ fontWeight: 600, fontSize: 14, width: 200 }}
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Projects
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <Link
                  className="dropdown-item"
                  to="/designs"
                  activeStyle={{
                    fontWeight: "semibold",
                    backgroundColor: "#FFEBB4",
                    padding: 12,
                    color: "#000000",
                  }}
                >
                  Design
                </Link>
                <div className="dropdown-divider"></div>
                <Link
                  className="dropdown-item"
                  to="/development"
                  activeStyle={{
                    fontWeight: "semibold",
                    backgroundColor: "#FFEBB4",
                    padding: 12,
                    color: "#000000",
                  }}
                >
                  Development
                </Link>
              </div>
            </div>
          </div>
        </div>

        {/* right side */}
      </div>

      {/*  */}
      <br />
      <br />
      <br />
      <br />

      <hr className="mt-5" />
      <Element name="footer">
        <Footer />
      </Element>
    </div>
  );
};

export default Homepage;
