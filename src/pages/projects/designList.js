import "../projects/designList.css";
import desktop2 from "../../assets/images/desktop2.png";
import desktop3 from "../../assets/images/desktop3.png";
import Desktop6 from "../../assets/images/Desktop6.png";
import { Link, useRouteMatch } from "react-router-dom";
import ScrollUpButton from "react-scroll-up-button";
import Footer from "../../components/Footer/Footer";
import * as Scroll from "react-scroll";

const DesignList = () => {
  let Element = Scroll.Element;
  let { url } = useRouteMatch();
  return (
    <div className="mt-5 mt-md-4 container">
      {/* heading */}
      <div>
        <h4 className="project-name text-center geek-head">Design projects</h4>
      </div>

      {/* card listing */}

      <div className="d-flex flex-column mt-5">
        <Link to={`${url}/geeks`}>
          <div className="m-auto w-75 img-contain">
            <img className="w-100" src={desktop2} alt="geeks" />
          </div>
        </Link>
      </div>

      {/*  */}
      <div className="d-flex flex-column mt-5">
        <Link to={`${url}/snek`}>
          <div className="m-auto w-75 img-contain">
            <img className="w-100" src={Desktop6} alt="snek" />
          </div>
        </Link>
      </div>

      {/*  */}
      <div className="d-flex flex-column mt-5">
        <Link to={`${url}/teenlearner`}>
          <div className="m-auto w-75 img-contain">
            <img className="w-100" src={desktop3} alt="teen" />
          </div>
        </Link>
      </div>

      {/*  */}
      <div>
        <ScrollUpButton />
      </div>

      {/*  */}
      <br />
      <br />
      <br />

      <hr className="mt-5" />
      <Element name="footer">
        <Footer />
      </Element>
    </div>
  );
};

export default DesignList;
